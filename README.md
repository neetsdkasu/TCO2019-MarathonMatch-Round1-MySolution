TCO2019 Marathon Match Round 1 My Solution
==========================================


Problem Statement:  
https://www.topcoder.com/challenges/30092483  


Standings:  
https://www.topcoder.com/challenges/30092483?tab=submissions  


Review Page (old):  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30092483  


Review Page (new):  
https://submission-review.topcoder.com/challenges/30092483   


Forum:  
https://apps.topcoder.com/forums/?module=Category&categoryID=69198  

