import java.io.*;
import java.util.*;

public class LineUp {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    int N, X;
    int[] heights, arrangements;

    long startTime;

    public int[] getLineup(int[] heights, int[] arrangements) {
        startTime = System.currentTimeMillis();
        long limitTime = startTime + 9000;
        this.heights = heights;
        this.arrangements = arrangements;
        N = heights.length;
        X = arrangements.length / N;
        System.err.printf("N: %d%n", N);
        System.err.printf("X: %d%n", X);

        int[] root2p = new int[N];
        int[] root2a = new int[N * (X + 1)];
        for (int i = 0; i < root2p.length; i++) {
            root2p[i] = i;
        }
        for (int i = 0; i < root2a.length; i++) {
            root2a[i] = i % N;
        }

        TpI2 sc = calcScoreFactors(root2p, root2a);
        System.err.printf("d: %d%n", sc.item1);
        System.err.printf("e: %d%n", sc.item2);
        System.err.printf("s: %f%n", score(sc.item1, sc.item2));

        // sc = setGreedy(root2p, root2a);
        // setRandom(root2a);
        // setRandomNearMost1(root2a);
        // setRandomNearMost2(root2a);
        // setMinErr(root2p, root2a);
        // setDpOnePerformers(root2p, root2a);
        // sc = calcScoreFactors(root2p, root2a);

        // System.err.printf("d0: %d%n", sc.item1);
        // System.err.printf("e0: %d%n", sc.item2);
        // System.err.printf("s0: %f%n", score(sc.item1, sc.item2));

        int[] rIdx = new int[8];
        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long interval = time1 - time0;
        long limitDpA8Forward = startTime + 2500;
        // for (int cycle = 0; cycle < 1000; cycle++) {
            // time0 = time1;
            // time1 = System.currentTimeMillis();
            // interval = Math.max(interval, time1 - time0);
            // if (time1 + interval > limitDpA8Forward) {
                // System.err.println("limit break");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
            // TpI2 tmp = sc;
            // for (int x = 1; x <= X; x++) {
                // for (int i = 0; i < rIdx.length; i++) {
                    // rIdx[i] = i;
                // }
                // for (int j = 0; j < N; j++) {
                    // rIdx[j & 7] = j;
                    // sc = dpA8Forward(root2p, root2a, x, rIdx, sc.item1, sc.item2);
                // }
            // }
            // if (tmp.item1 == sc.item1 && tmp.item2 == sc.item2) {
                // System.err.println("no update");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
        // }

        // System.err.printf("interval: %d ms%n", interval);
        // System.err.printf("d3: %d%n", sc.item1);
        // System.err.printf("e3: %d%n", sc.item2);
        // System.err.printf("s3: %f%n", score(sc.item1, sc.item2));

        // time1 = System.currentTimeMillis();
        // interval = 0;
        // long limitDpA8Liner = Math.min(startTime + 2500, time1 + 2500);
        // for (int cycle = 0; cycle < 1000; cycle++) {
            // time0 = time1;
            // time1 = System.currentTimeMillis();
            // interval = Math.max(interval, time1 - time0);
            // if (time1 + interval > limitDpA8Liner) {
                // System.err.println("limit break");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
            // TpI2 tmp = sc;
            // for (int x = 1; x <= X; x++) {
                // for (int i = 0; i < rIdx.length; i++) {
                    // rIdx[i] = i;
                // }
                // for (int j = 0; j < N; j++) {
                    // rIdx[j & 7] = j;
                    // sc = dpA8(root2p, root2a, x, rIdx, sc.item1, sc.item2);
                // }
            // }
            // if (tmp.item1 == sc.item1 && tmp.item2 == sc.item2) {
                // System.err.println("no update");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
        // }

        // System.err.printf("interval: %d ms%n", interval);
        // System.err.printf("d1: %d%n", sc.item1);
        // System.err.printf("e1: %d%n", sc.item2);
        // System.err.printf("s1: %f%n", score(sc.item1, sc.item2));



        // time1 = System.currentTimeMillis();
        // interval = 0;
        // long limitDpA8Root = Math.min(startTime + 7500, time1 + 2500);
        // for (int cycle = 0; cycle < 1000; cycle++) {
            // time0 = time1;
            // time1 = System.currentTimeMillis();
            // interval = Math.max(interval, time1 - time0);
            // if (time1 + interval > limitTime) {
                // System.err.println("limit break");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
            // TpI2 tmp = sc;
            // for (int x = X; x > 0; x--) {
                // for (int i = 0; i < 8; i++) {
                    // rIdx[i] = i;
                // }
                // for (int i = 0; i < N; i++) {
                    // rIdx[i & 7] = i;
                    // sc = dp8Roots(root2p, root2a, x, rIdx, sc.item1, sc.item2);
                // }
            // }
            // if (tmp.item1 == sc.item1 && tmp.item2 == sc.item2) {
                // System.err.println("no update");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
        // }

        // System.err.printf("interval: %d ms%n", interval);
        // System.err.printf("d4: %d%n", sc.item1);
        // System.err.printf("e4: %d%n", sc.item2);
        // System.err.printf("s4: %f%n", score(sc.item1, sc.item2));



        // rIdx = new int[N];
        // for (int i = 0; i < N; i++) {
            // rIdx[i] = i;
        // }
        // time1 = System.currentTimeMillis();
        // interval = 0;
        // for (int cycle = 0; cycle < 10000000; cycle++) {
            // time0 = time1;
            // time1 = System.currentTimeMillis();
            // interval = Math.max(interval, time1 - time0);
            // if (time1 + interval > limitTime) {
                // System.err.println("limit break");
                // System.err.printf("cycle: %d%n", cycle);
                // break;
            // }
            // for (int j = 0; j < 100; j++) {
                // int x = Util.rand.nextInt(X) + 1;
                // for (int i = 0; i < 8; i++) {
                    // int p = Util.nextRange(i, N);
                    // Util.swapElements(rIdx, i, p);
                // }
                // if (Util.rand.nextInt(100) < 50) {
                    // sc = dpA8(root2p, root2a, x, rIdx, sc.item1, sc.item2);
                // } else {
                    // sc = dp8Roots(root2p, root2a, x, rIdx, sc.item1, sc.item2);
                // }
            // }
        // }

        // System.err.printf("interval: %d ms%n", interval);
        // System.err.printf("d2: %d%n", sc.item1);
        // System.err.printf("e2: %d%n", sc.item2);
        // System.err.printf("s2: %f%n", score(sc.item1, sc.item2));


        sc = sa(root2p, root2a, sc.item1, sc.item2, limitTime);

        // System.err.printf("interval: %d ms%n", interval);
        System.err.printf("d5: %d%n", sc.item1);
        System.err.printf("e5: %d%n", sc.item2);
        System.err.printf("s5: %f%n", score(sc.item1, sc.item2));



        int[] ret = new int[N * X];
        for (int j  = 0; j < X; j++) {
            for (int i = 0; i < N; i++) {
                int p = root2p[i];
                int a = root2a[(j + 1) * N + i];
                ret[j * N + a] = p;
            }
        }
        return ret;
    }

    TpI2 calcScoreFactors(int[] root2p, int[] root2a) {
        int dist = 0, err = 0;
        for (int i = 0; i < N; i++) {
            int p = root2p[i];
            int h = heights[p];
            for (int j = 0; j < X; j++) {
                int a = root2a[(j + 1) * N + i];
                int e = arrangements[j * N + a] - h;
                err += e * e;
                dist += Math.abs(p - a);
                p = a;
            }
        }
        return new TpI2(dist, err);
    }

    static final int[][] dpA8Indexes = new int[8][];
    static {
        dpA8Indexes[0] = new int[1];
        boolean[] flag = new boolean[256];
        int c = 1;
        for (int i = 1; i < 8; i++) {
            c *= 8 - (i - 1);
            c /= i;
            int[] tmp = new int[c];
            int p = 0;
            for (int x : dpA8Indexes[i - 1]) {
                for (int j = 0; j < 8; j++) {
                    int k = x | (1 << j);
                    if (k != x && !flag[k]) {
                        flag[k] = true;
                        tmp[p] = k;
                        p++;
                    }
                }
            }
            dpA8Indexes[i] = tmp;
        }
    }

    static final DpA8[] dpA8Table = new DpA8[256];
    static {
        for (int i = 0; i < dpA8Table.length; i++) {
            dpA8Table[i] = new DpA8(0, 0, 0.0);
        }
    }
    static DpA8[] getDpA8Table() {
        for (int i = 0; i < dpA8Table.length; i++) {
            dpA8Table[i].score = 1e10;
        }
        return dpA8Table;
    }

    TpI2 dpA8(int[] root2p, int[] root2a, int x, int[] rIdx, int dist, int err) {
        if (x == X) {
            return dpA8X(root2p, root2a, rIdx, dist, err);
        }
        int xN = x * N, xm1N = (x - 1) * N, xp1N = (x + 1) * N;
        DpA8[] dp = getDpA8Table();
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            int p = root2p[r];
            int a = root2a[xN + r];
            int e = arrangements[xm1N + a] - heights[p];
            err -= e * e;
            dist -= Math.abs(root2a[xm1N + r] - a)
                + Math.abs(root2a[xp1N + r] - a);
        }
        dp[0].set(dist, err, score(dist, err));
        for (int di = 0; di < 8; di++) {
            int rp = rIdx[di];
            int h = heights[root2p[rp]];
            int posPrev = root2a[xm1N + rp];
            int posNext = root2a[xp1N + rp];
            int[] indexes = dpA8Indexes[di];
            for (int ii = 0; ii < indexes.length; ii++) {
                int idx = indexes[ii];
                DpA8 dp0 = dp[idx];
                int dist0 = dp0.dist;
                int err0 = dp0.err;
                for (int i = 0; i < 8; i++) {
                    int k = idx | (1 << i);
                    if (k == idx) {
                        continue;
                    }
                    int r = rIdx[i];
                    int a = root2a[xN + r];
                    int e = arrangements[xm1N + a] - h;
                    e = err0 + e * e;
                    int d = dist0 + Math.abs(posPrev - a) + Math.abs(posNext - a);
                    double s = score(d, e);
                    DpA8 tmp = dp[k];
                    if (s < tmp.score) {
                        tmp.set(d, e, s);
                        tmp.set(dp0, di, a);
                    }
                }
            }
        }
        DpA8 ans = dp[255];
        for (int i = 0; i < 8; i++) {
            root2a[xN + rIdx[i]] = ans.ps[i];
        }
        return new TpI2(ans.dist, ans.err);
    }

    TpI2 dpA8X(int[] root2p, int[] root2a, int[] rIdx, int dist, int err) {
        int xN = X * N, xm1N = (X - 1) * N;
        DpA8[] dp = getDpA8Table();
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            int p = root2p[r];
            int a = root2a[xN + r];
            int e = arrangements[xm1N + a] - heights[p];
            err -= e * e;
            dist -= Math.abs(root2a[xm1N + r] - a);
        }
        dp[0].set(dist, err, score(dist, err));
        for (int di = 0; di < 8; di++) {
            int rp = rIdx[di];
            int h = heights[root2p[rp]];
            int posPrev = root2a[xm1N + rp];
            int[] indexes = dpA8Indexes[di];
            for (int ii = 0; ii < indexes.length; ii++) {
                int idx = indexes[ii];
                DpA8 dp0 = dp[idx];
                int dist0 = dp0.dist;
                int err0 = dp0.err;
                for (int i = 0; i < 8; i++) {
                    int k = idx | (1 << i);
                    if (k == idx) {
                        continue;
                    }
                    int r = rIdx[i];
                    int a = root2a[xN + r];
                    int e = arrangements[xm1N + a] - h;
                    e = err0 + e * e;
                    int d = dist0 + Math.abs(posPrev - a);
                    double s = score(d, e);
                    DpA8 tmp = dp[k];
                    if (s < tmp.score) {
                        tmp.set(d, e, s);
                        tmp.set(dp0, di, a);
                    }
                }
            }
        }
        DpA8 ans = dp[255];
        for (int i = 0; i < 8; i++) {
            root2a[xN + rIdx[i]] = ans.ps[i];
        }
        return new TpI2(ans.dist, ans.err);
    }

    TpI2 dpA8Forward(int[] root2p, int[] root2a, int x, int[] rIdx, int dist, int err) {
        int xN = x * N, xm1N = (x - 1) * N, xp1N = (x + 1) * N;
        if (x == X) {
            xp1N = xN;
        }
        DpA8[] dp = getDpA8Table();
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            int p = root2p[r];
            int a = root2a[xN + r];
            int e = arrangements[xm1N + a] - heights[p];
            err -= e * e;
            dist -= Math.abs(root2a[xm1N + r] - a) + Math.abs(root2a[xp1N + r] - a);
        }
        dp[0].set(dist, err, score(dist, err));
        for (int di = 0; di < 8; di++) {
            int rp = rIdx[di];
            int h = heights[root2p[rp]];
            int posPrev = root2a[xm1N + rp];
            int[] indexes = dpA8Indexes[di];
            for (int ii = 0; ii < indexes.length; ii++) {
                int idx = indexes[ii];
                DpA8 dp0 = dp[idx];
                int dist0 = dp0.dist;
                int err0 = dp0.err;
                for (int i = 0; i < 8; i++) {
                    int k = idx | (1 << i);
                    if (k == idx) {
                        continue;
                    }
                    int r = rIdx[i];
                    int a = root2a[xN + r];
                    int e = arrangements[xm1N + a] - h;
                    e = err0 + e * e;
                    int d = dist0 + Math.abs(posPrev - a);
                    double s = score(d, e);
                    DpA8 tmp = dp[k];
                    if (s < tmp.score) {
                        tmp.set(d, e, s);
                        tmp.set(dp0, di, a);
                    }
                }
            }
        }
        DpA8 ans = dp[255];
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            root2a[xN + r] = ans.ps[i];
            ans.dist += Math.abs(root2a[xN + r] - root2a[xp1N + r]);
        }
        return new TpI2(ans.dist, ans.err);
    }

    TpI2 dp8Roots(int[] root2p, int[] root2a, int x, int[] rIdx, int dist, int err) {
        int xN = x * N, xm1N = xN - N;
        int sz = X - x + 1;
        TpI2[] ars = new TpI2[sz * 8];
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            int xn = xN;
            int xm1n = xn - N;
            int ai = i;
            for (int tx = x; tx <= X; tx++) {
                int a = root2a[xn + r];
                ars[ai] = new TpI2(a, arrangements[xm1n + a]);
                ai += 8;
                xm1n = xn;
                xn += N;
            }
        }
        TpI2[] scs = new TpI2[64];
        for (int i = 0; i < 8; i++) {
            int rp = rIdx[i];
            int h = heights[root2p[rp]];
            int a0 = root2a[xm1N + rp];
            for (int j = 0; j < 8; j++) {
                int d1 = 0;
                int e1 = 0;
                int a = a0;
                for (int ai = j; ai < ars.length; ai += 8) {
                    TpI2 tp = ars[ai];
                    int ta = tp.item1;
                    int e = tp.item2 - h;
                    d1 += Math.abs(ta - a);
                    e1 += e * e;
                    a = ta;
                }
                scs[i * 8 + j] = new TpI2(d1, e1);
                if (i == j) {
                    dist -= d1;
                    err -= e1;
                }
            }
        }
        DpA8[] dp = getDpA8Table();
        dp[0].set(dist, err, score(dist, err));
        for (int di = 0; di < 8; di++) {
            int[] indexes = dpA8Indexes[di];
            for (int ii = 0; ii < indexes.length; ii++) {
                int idx = indexes[ii];
                DpA8 dp0 = dp[idx];
                int dist0 = dp0.dist;
                int err0 = dp0.err;
                for (int i = 0; i < 8; i++) {
                    int k = idx | (1 << i);
                    if (k == idx) {
                        continue;
                    }
                    TpI2 sc = scs[di * 8 + i];
                    int d = dist0 + sc.item1;
                    int e = err0 + sc.item2;
                    double s = score(d, e);
                    DpA8 tmp = dp[k];
                    if (s < tmp.score) {
                        tmp.set(d, e, s);
                        tmp.set(dp0, di, i);
                    }
                }
            }
        }
        DpA8 ans = dp[255];
        for (int i = 0; i < 8; i++) {
            int r = rIdx[i];
            int ai = ans.ps[i];
            int xnr = xN + r;
            for (int tx = x; tx <= X; tx++) {
                root2a[xnr] = ars[ai].item1;
                xnr += N;
                ai += 8;
            }
        }
        return new TpI2(ans.dist, ans.err);
    }

    static final double initialTemperature = 1.5;
    static final double coolDown = 0.987654;
    static final int MINI_CYCLE = 10000;

    static int pow2(int a) {
        return a * a;
    }

    // boolean accept(double tmpScore, double curScore, double temperature, long curTime, long limitTime) {
    boolean accept(double tmpScore, double curScore, double temperature) {
        if (tmpScore > curScore) {
            // double t = Math.pow(initialTemperature, (double)(curTime - startTime) / (double)(limitTime - startTime));
            double s = Math.pow(Math.E, (curScore - tmpScore) / temperature);
            return Util.rand.nextDouble() <= s;
        }
        return true;
    }

    TpI2 sa(int[] root2p, int[] root2a, int dist, int err, long limitTime) {
        int[] root = Arrays.copyOf(root2a, root2a.length);
        int bestDist = dist, bestErr = err;
        double bestScore = score(bestDist, bestErr);
        int curDist = bestDist, curErr = bestErr;
        double curScore = bestScore;
        long time0 = System.currentTimeMillis();
        long time1 = time0;
        long interval = 0;
        double temperature = initialTemperature;
        for (long cycle = 0L; ; cycle++) {
            time0 = time1;
            time1 = System.currentTimeMillis();
            interval = Math.max(interval, time1 - time0);
            if (time1 + interval > limitTime) {
                System.err.printf("cycle: %d%n", cycle);
                break;
            }
            double beforeScore = bestScore;
            temperature *= coolDown;
            for (int miniCycle = 0; miniCycle < MINI_CYCLE; miniCycle++) {
                int x = Util.rand.nextInt(X) + 1;
                int r1 = Util.rand.nextInt(N);
                int r2 = r1 + Util.nextRange(1, N);
                if (r2 >= N) { r2 -= N; }
                int sel = Util.rand.nextInt(2);
                int h1 = heights[root2p[r1]];
                int h2 = heights[root2p[r2]];
                if (sel == 0) {
                    int prevA1 = root[(x - 1) * N + r1];
                    int prevA2 = root[(x - 1) * N + r2];
                    int a1 = root[x * N + r1];
                    int a2 = root[x * N + r2];
                    int arr1 = arrangements[(x - 1) * N + a1];
                    int arr2 = arrangements[(x - 1) * N + a2];
                    int bfErr = pow2(arr1 - h1) + pow2(arr2 - h2);
                    int afErr = pow2(arr2 - h1) + pow2(arr1 - h2);
                    int bfDist = Math.abs(prevA1 - a1) + Math.abs(prevA2 - a2);
                    int afDist = Math.abs(prevA2 - a1) + Math.abs(prevA1 - a2);
                    if (x < X) {
                        int nextA1 = root[(x + 1) * N + r1];
                        int nextA2 = root[(x + 1) * N + r2];
                        bfDist += Math.abs(nextA1 - a1) + Math.abs(nextA2 - a2);
                        afDist += Math.abs(nextA2 - a1) + Math.abs(nextA1 - a2);
                    }
                    int tmpDist = curDist + afDist - bfDist;
                    int tmpErr = curErr + afErr - bfErr;
                    double tmpScore = score(tmpDist, tmpErr);
                    // if (accept(tmpScore, curScore, temperature, time1, limitTime)) {
                    if (accept(tmpScore, curScore, temperature)) {
                        root[x * N + r1] = a2;
                        root[x * N + r2] = a1;
                        curDist = tmpDist;
                        curErr = tmpErr;
                        curScore = tmpScore;
                        if (curScore < bestScore) {
                            bestDist = curDist;
                            bestErr = curErr;
                            bestScore = curScore;
                            System.arraycopy(root, 0, root2a, 0, root2a.length);
                        }
                    }
                } else {
                    int prevA1 = root[(x - 1) * N + r1];
                    int prevA2 = root[(x - 1) * N + r2];
                    int nextA1 = root[x * N + r1];
                    int nextA2 = root[x * N + r2];
                    int bfDist = Math.abs(prevA1 - nextA1) + Math.abs(prevA2 - nextA2);
                    int afDist = Math.abs(prevA2 - nextA1) + Math.abs(prevA1 - nextA2);
                    int bfErr = 0;
                    int afErr = 0;
                    for (int tx = x; tx <= X; tx++) {
                        int a1 = root[tx * N + r1];
                        int a2 = root[tx * N + r2];
                        int arr1 = arrangements[(tx - 1) * N + a1];
                        int arr2 = arrangements[(tx - 1) * N + a2];
                        bfErr += pow2(arr1 - h1) + pow2(arr2 - h2);
                        afErr += pow2(arr2 - h1) + pow2(arr1 - h2);
                    }
                    int tmpDist = curDist + afDist - bfDist;
                    int tmpErr = curErr + afErr - bfErr;
                    double tmpScore = score(tmpDist, tmpErr);
                    // if (accept(tmpScore, curScore, temperature, time1, limitTime)) {
                    if (accept(tmpScore, curScore, temperature)) {
                        for (int tx = x; tx <= X; tx++) {
                            Util.swapElements(root, tx * N + r1, tx * N + r2);
                        }
                        curDist = tmpDist;
                        curErr = tmpErr;
                        curScore = tmpScore;
                        if (curScore < bestScore) {
                            bestDist = curDist;
                            bestErr = curErr;
                            bestScore = curScore;
                            System.arraycopy(root, 0, root2a, 0, root2a.length);
                        }
                    }
                }
            }
            if (bestScore < beforeScore) {
                temperature /= coolDown;
            }
        }
        System.err.printf("interval: %d ms%n", interval);
        return new TpI2(bestDist, bestErr);
    }

    TpI2 setGreedy(int[] root2p, int[] root2a) {
        int[] indexesR = new int[N];
        int[] indexesA = new int[N];
        for (int i = 0; i < N; i++) {
            indexesR[i] = i;
            indexesA[i] = i;
            root2a[i] = i;
            root2p[i] = i;
        }
        int dist = 0, err = 0;
        int xN = 0, xm1N = 0;
        for (int x = 1; x <= X; x++) {
            xm1N = xN;
            xN += N;
            for (int cnt = N; cnt > 0; cnt--) {
                int minDist = 0, minErr = 0, minRi = 0, minAi = 0;
                double minScore = 1e10;
                for (int ri = 0; ri < cnt; ri++) {
                    int r = indexesR[ri];
                    int h = heights[r];
                    int pa = root2a[xm1N + r];
                    for (int ai = 0; ai < cnt; ai++) {
                        int a = indexesA[ai];
                        int d = dist + Math.abs(pa - a);
                        int e = arrangements[xm1N + a] - h;
                        e = err + e * e;
                        double s = score(d, e);
                        if (s < minScore) {
                            minDist = d;
                            minErr = e;
                            minScore = s;
                            minRi = ri;
                            minAi = ai;
                        }
                    }
                }
                root2a[xN + indexesR[minRi]] = indexesA[minAi];
                Util.swapElements(indexesR, minRi, cnt - 1);
                Util.swapElements(indexesA, minAi, cnt - 1);
                dist = minDist;
                err = minErr;
            }
        }
        return new TpI2(dist, err);
    }

    void setRandom(int[] root2a) {
        int xN = 0;
        for (int x = 1; x <= X; x++) {
            xN += N;
            Util.shuffle(root2a, xN, N);
        }
    }

    void setRandomNearMost1(int[] root2a) {
        int xN = 0, xm1N = 0;
        int[] arr = new int[N];
        for (int x = 0; x < X; x++) {
            xm1N = xN;
            xN += N;
            for (int i = 0; i < N; i++) {
                if (i + 1 < N && Util.rand.nextInt(100) < 50) {
                    arr[i] = i + 1;
                    arr[i + 1] = i;
                    i++;
                } else {
                    arr[i] = i;
                }
            }
            for (int i = 0; i < N; i++) {
                root2a[xN + i] = arr[root2a[xm1N + i]];
            }
        }
    }

    void setRandomNearMost2(int[] root2a) {
        int xN = 0, xm1N = 0;
        int[] arr = new int[N];
        for (int x = 0; x < X; x++) {
            xm1N = xN;
            xN += N;
            for (int i = 0; i < N; i++) {
                int rnd = Util.rand.nextInt(200);
                int sel = 0;
                if (i + 1 < N && rnd < 100) {
                    if (rnd < 50) {
                        sel = 1;
                    } else {
                        if (i + 3 < N) {
                            if (rnd < 75) {
                                sel = 2;
                            } else {
                                sel = 3;
                            }
                        } else if (i + 2 < N) {
                            sel = 2;
                        } else {
                            sel = 1;
                        }
                    }
                }
                if (sel == 0) {
                    arr[i] = i;
                } else if (sel == 1) {
                    arr[i] = i + 1;
                    arr[i + 1] = i;
                    i++;
                } else if (sel == 2) {
                    arr[i] = i + 2;
                    arr[i + 1] = i + 1;
                    arr[i + 2] = i;
                    i += 2;
                } else {
                    arr[i] = i + 2;
                    arr[i + 1] = i + 3;
                    arr[i + 2] = i;
                    arr[i + 3] = i + 1;
                    i += 3;
                }
            }
            for (int i = 0; i < N; i++) {
                root2a[xN + i] = arr[root2a[xm1N + i]];
            }
        }
    }

    void setMinErr(int[] root2p, int[] root2a) {
        int[] hs = new int[N];
        int[] ps = new int[N];
        for (int x = 0; x < X; x++) {
            for (int i = 0; i < N; i++) {
                int p = root2p[i];
                hs[i] = heights[p] * 10000 + root2a[x * N + i] * 100 + i;
                ps[i] = arrangements[x * N + i] * 100 + i;
            }
            Arrays.sort(hs);
            Arrays.sort(ps);
            for (int i = 0; i < N; i++) {
                root2a[(x + 1) * N + hs[i] % 100] = ps[i] % 100;
            }
        }
    }

    void setDpOnePerformers(int[] root2p, int[] root2a) {
        for (int i = 0; i < N; i++) {
            root2a[i] = i;
            root2p[i] = i;
        }
        for (int i = N; i < root2a.length; i++) {
            root2a[i] = -1;
        }
        boolean[] flag = new boolean[N];
        boolean[] used = new boolean[X * N];
        DpOp[][] dps = new DpOp[N][X * N];
        int remain = N;
        int dist = 0, err = 0;
        for (int lp = 0; remain > 0 && lp < N; lp++) {
            for (int p = 0; p < N; p++) {
                if (flag[p]) {
                    continue;
                }
                DpOp[] dp = dps[p];
                for (int i = 0; i < dp.length; i++) {
                    if (dp[i] == null) {
                        dp[i] = new DpOp();
                    }
                    dp[i].set(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2, 1e10, 0);
                }
                int h = heights[p];
                for (int i = 0; i < N; i++) {
                    if (used[i]) {
                        continue;
                    }
                    int d = dist + Math.abs(i - p);
                    int e = arrangements[i] - h;
                    e = err + e * e;
                    dp[i].set(d, e, score(d, e), p);
                }
                int xN = 0, xm1N = 0;
                for (int x = 1; x < X; x++) {
                    xm1N = xN;
                    xN += N;
                    for (int i = 0; i < N; i++) {
                        if (used[xm1N + i]) {
                            continue;
                        }
                        DpOp tmp = dp[xm1N + i];
                        int dist0 = tmp.dist, err0 = tmp.err;
                        for (int j = 0; j < N; j++) {
                            if (used[xN + j]) {
                                continue;
                            }
                            int d = dist0 + Math.abs(j - i);
                            int e = arrangements[xN + j] - h;
                            e = err0 + e * e;
                            double s = score(d, e);
                            if (s < dp[xN + j].score) {
                                dp[xN + j].set(d, e, s, i);
                            }
                        }
                    }
                }
            }
            TpDI[] targets = new TpDI[N * N];
            int xn = (X - 1) * N;
            for (int p = 0; p < N; p++) {
                DpOp[] dp = dps[p];
                for (int i = 0; i < N; i++) {
                    targets[p * N + i] = new TpDI(dp[xn + i].score, p * N + i);
                }
            }
            Arrays.sort(targets);
            for (int k = 0; k < targets.length; k++) {
                int pi = targets[k].item2;
                int p = pi / N, i = pi % N;
                if (flag[p] || used[(X - 1) * N + i]) {
                    continue;
                }
                boolean ok = true;
                int ti = i;
                DpOp[] dp = dps[p];
                for (int x = X - 1; x > 0; x--) {
                    ti = dp[x * N + ti].prev;
                    if (used[(x - 1) * N + ti]) {
                        ok = false;
                        break;
                    }
                }
                if (!ok) {
                    continue;
                }
                flag[p] = true;
                remain--;
                used[(X - 1) * N + i] = true;
                root2a[X * N + i] = p;
                dist += dp[(X - 1) * N + i].dist;
                err += dp[(X - 1) * N + i].err;
                for (int x = X - 1; x > 0; x--) {
                    i = dp[x * N + i].prev;
                    root2a[x * N + i] = p;
                    used[(x - 1) * N + i] = true;
                }
            }
        }
        for (int x = 1; x <= X; x++) {
            int p = 0;
            while (p < N && flag[p]) {
                p++;
            }
            for (int i = 0; i < N; i++) {
                if (root2a[x * N + i] < 0) {
                    root2a[x * N + i] = p;
                    do {
                        p++;
                    } while (p < N && flag[p]);
                }
            }
        }
    }

    static double score(int dist, int err) {
        return (double)dist + Math.sqrt((double)err);
    }
}

final class Util {
    private Util() {}

    public static final Random rand = new Random(1983_1983_1983_1983L);

    public static void swapElements(int[] vs, int i, int j) {
        int tmp = vs[i];
        vs[i] = vs[j];
        vs[j] = tmp;
    }

    public static void shuffle(int[] vs, int off, int len) {
        for (int i = len - 1; i > 0; i--) {
            int p = rand.nextInt(i + 1);
            swapElements(vs, p + off, i + off);
        }
    }

    public static int nextRange(int s, int e) {
        return rand.nextInt(e - s) + s;
    }
}

class TpI2 {
    public final int item1, item2;
    TpI2(int i1, int i2) {
        item1 = i1; item2 = i2;
    }
}

class TpDI implements Comparable<TpDI> {
    public final double item1;
    public final int item2;
    TpDI(double i1, int i2) {
        item1 = i1; item2 = i2;
    }
    public int compareTo(TpDI other) {
        int ret = Double.compare(item1, other.item1);
        if (ret == 0) {
            return Integer.compare(item2, other.item2);
        } else {
            return ret;
        }
    }
}

class DpOp {
    int dist, err;
    double score = 1e10;
    int prev;
    void set(int d, int e, double s, int p) {
        dist = d; e = err; score = s; prev = p;
    }
}

class DpA8 {
    int dist, err;
    double score;
    int[] ps = new int[8];
    DpA8(int d, int e, double s) {
        set(d, e, s);
    }
    void set(int d, int e, double s) {
        dist = d;
        err = e;
        score = s;
    }
    void set(DpA8 other, int di, int a) {
        System.arraycopy(other.ps, 0, ps, 0, di);
        ps[di] = a;
    }
}

class Main {

    static int callGetLineup(BufferedReader in, LineUp lineUp) throws Exception {

        long startTime = System.currentTimeMillis();

        int _heightsSize = Integer.parseInt(in.readLine());
        int[] heights = new int[_heightsSize];
        for (int _idx = 0; _idx < _heightsSize; _idx++) {
            heights[_idx] = Integer.parseInt(in.readLine());
        }
        int _arrangementsSize = Integer.parseInt(in.readLine());
        int[] arrangements = new int[_arrangementsSize];
        for (int _idx = 0; _idx < _arrangementsSize; _idx++) {
            arrangements[_idx] = Integer.parseInt(in.readLine());
        }

        int[] _result = lineUp.getLineup(heights, arrangements);

        StringBuilder sb = new StringBuilder();
        sb.append(_result.length);
        for (int _it : _result) {
            sb.append(System.lineSeparator());
            sb.append(_it);
        }
        String ret = sb.toString();

        long endTime = System.currentTimeMillis();
        System.err.printf("time: %d ms%n", endTime - startTime);

        System.out.println(ret);
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        LineUp lineUp = new LineUp();

        // do edit codes if you need

        callGetLineup(in, lineUp);

    }

}
